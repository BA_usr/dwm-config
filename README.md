# DWM config

Это мой репозиторий с настраиваемой конфигурацией dwm.

## Getting started

Установите нужные зависимости если вы используете arch linux: 
sudo pacman -S imlib2 xorg-xinit xorg-server git base-devel libx11 libxft libxinerama freetype2 fontconfig xdg-dbus-proxy

Установите нужные зависимости если вы используете void linux: 
sudo xbps-install base-devel libX11-devel libXft-devel libXinerama-devel freetype-devel fontconfig-devel git

Клонируйте исходный код из репозитория точечных файлов dwm: 
git clone https://gitlab.com/BA_usr/dwm-config.git

## Какие патчи используются для dwm

[fullgaps](https://dwm.suckless.org/patches/fullgaps/) - этот патч добавляет промежутки между окнами клиента.  Аналогичен gaps, но содержит дополнительный функционал:
- добавляет внешние зазоры (между клиентами и рамкой экрана), а также зазор между мастером и областью стека,
- добавляет сочетания клавиш для изменения размера зазора во время выполнения: [Alt]+[-]/[Alt]+[=] для уменьшения/увеличения размера зазора и [Alt]+[Shift]+[=] для установки его на ноль.

[tag previews](https://dwm.suckless.org/patches/tag-previews/) - данный патч позволяет увидеть содержимое уже используемого тега.

[moveresize](https://dwm.suckless.org/patches/moveresize/) - это патч позволяет измененять, перемещать и изменять размер клиентов dwm с помощью привязок клавиатуры.

[aspectresize](https://dwm.suckless.org/patches/aspectresize/) - этот патч позволяет изменить размер окна с постоянным соотношением сторон, используйте патч moveresize для ручного изменения размера.

[next prev tag](https://dwm.suckless.org/patches/nextprev/) - увеличение или уменьшение выбранного тега

[dwm-shiftviewclients](https://github.com/bakkeby/patches/blob/master/dwm/dwm-shiftviewclients-6.2.diff) - увеличение или уменьшение выбранного тега

[alwayscenter](https://dwm.suckless.org/patches/alwayscenter/) - Все плавающие окна располагаются по центру, как центральная заплатка, но без правила.

